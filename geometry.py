import numpy as np


class Point2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def to_vector(self):
        return [self.x, self.y]


class Point3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.w = 1

    def to_vector(self):
        return [self.x, self.y, self.z]

    def out_of_camera(self):
        return self.z <= 0

    def update_from_vector(self, vector):
        self.x = vector[0]
        self.y = vector[1]
        self.z = vector[2]
        self.w = vector[3]


class Polygon:
    def __init__(self, vectors, color):
        self.vectors = vectors
        self.color = color
        self.points = self.set_points()

    def set_points(self):
        return [pt for x in self.vectors for pt in x]

    def dist_to_observator(self):
        pt = Point3D(np.sum(map(lambda point: point.x, self.points))/len(self.points),
                   np.sum(map(lambda point: point.y, self.points))/len(self.points),
                   np.sum(map(lambda point: point.z, self.points))/len(self.points))
        distance = np.sqrt(pt.x**2 + pt.y**2 + pt.z**2)
        return distance

    def max_z(self):
        return np.max(map(lambda point: point[2], self.points))

    def min_z(self):
        return np.min(map(lambda point: point[2], self.points))


class Vector:
    def __init__(self, a, b):
        self.a = a
        self.b = b


class Cuboid:
    def __init__(self, x, y, z, width, height, color):
        self.x = x
        self.y = y
        self.z = z
        self.width = width
        self.height = height
        self.color = color
        self.polygons = []
        self.create_walls()

    def create_walls(self):
        self.create_front_wall()
        self.create_back_wall()
        self.create_bottom_wall()
        self.create_top_wall()
        self.create_left_wall()
        self.create_right_wall()

    def create_front_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y, self.z)
                              , Point3D(self.x + self.width, self.y, self.z)))

        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z),
                              Point3D(self.x + self.width, self.y + self.height, self.z)))

        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z),
                              Point3D(self.x, self.y + self.height, self.z)))

        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z),
                              Point3D(self.x, self.y, self.z)))

        self.polygons.append(Polygon(vectors, self.color))

    def create_back_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y, self.z + self.height),
                              Point3D(self.x + self.width, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z + self.height),
                              Point3D(self.x + self.width, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z + self.height),
                              Point3D(self.x, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z + self.height),
                              Point3D(self.x, self.y, self.z + self.height)))
        self.polygons.append(Polygon(vectors, self.color))

    def create_bottom_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y, self.z),
                              Point3D(self.x, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y, self.z + self.height),
                              Point3D(self.x + self.width, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z + self.height),
                              Point3D(self.x + self.width, self.y, self.z)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z),
                              Point3D(self.x, self.y, self.z)))
        self.polygons.append(Polygon(vectors, self.color))

    def create_top_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z),
                              Point3D(self.x, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z + self.height),
                              Point3D(self.x + self.width, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z + self.height),
                              Point3D(self.x + self.width, self.y + self.height, self.z)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z),
                              Point3D(self.x, self.y + self.height, self.z)))
        self.polygons.append(Polygon(vectors, self.color))

    def create_left_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y, self.z),
                              Point3D(self.x, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y, self.z + self.height),
                              Point3D(self.x, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z + self.height),
                              Point3D(self.x, self.y + self.height, self.z)))
        vectors.append(Vector(Point3D(self.x, self.y + self.height, self.z),
                              Point3D(self.x, self.y, self.z)))
        self.polygons.append(Polygon(vectors, self.color))

    def create_right_wall(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z),
                              Point3D(self.x + self.width, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z + self.height),
                              Point3D(self.x + self.width, self.y + self.height, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z + self.height),
                              Point3D(self.x + self.width, self.y + self.height, self.z)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y + self.height, self.z),
                              Point3D(self.x + self.width, self.y, self.z)))
        self.polygons.append(Polygon(vectors, self.color))


class Street:
    def __init__(self, x, y, z, width, height, color):
        self.x = x
        self.y = y
        self.z = z
        self.width = width
        self.height = height
        self.color = color
        self.polygons = self.create_street_polygons()

    def create_street_polygons(self):
        vectors = []
        vectors.append(Vector(Point3D(self.x, self.y, self.z),
                              Point3D(self.x + self.width, self.y, self.z)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z),
                              Point3D(self.x + self.width, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x + self.width, self.y, self.z + self.height),
                              Point3D(self.x, self.y, self.z + self.height)))
        vectors.append(Vector(Point3D(self.x, self.y, self.z + self.height),Point3D(self.x, self.y, self.z)))
        return Polygon(vectors, self.color)


class Scene:
    def __init__(self, figures):
        self.vpd = 200
        self.polygons = self.set_polygons(figures)
        self.vectors = self.set_vectors()

    def zoom_out(self):
        if self.vpd < 0:
            print('Exception: VPD < 0')
            self.vpd = 5
        else:
            self.vpd -= 5

    def zoom_in(self):
        self.vpd += 5

    def set_polygons(self, figures):
        return [polygon for polygon in figures]

    def set_vectors(self):
        return [vector for polygon in self.polygons for vector in polygon]

    def point_to_2D(self, point):
        new_x = point.x * self.vpd / point.z
        new_y = point.y * self.vpd / point.z
        return Point2D(new_x, new_y)



    def make_projection(self):
        polygons = []

        for polygon in self.polygons:
            vectors2D = []

            for vector in polygon.vectors:
                if vector.a.out_of_camera() or vector.b.out_of_camera():
                    return
                vectors2D.append(Vector(self.point_to_2D(vector.a), self.point_to_2D(vector.b)))
            polygons.append(Polygon(vectors2D, polygon.color))

        return polygons



    #
    #   runPaintersAlgorithm() {
    #     this._polygons.sort(PaintersAlgorithm.compare);
    #   }
    #
    #   draw() {
    #     this.runPaintersAlgorithm();
    #     let polygons = this.makeProjection();
    #
    #     this.ctx.clearRect(0, 0, this.c.width, this.c.height);
    #
    #     polygons.forEach((polygon) => {
    #       let [first, ...tail] = polygon.points;
    #       if (!first) return;
    #
    #       this.ctx.fillStyle = polygon.color;
    #       this.ctx.beginPath();
    #
    #       this.ctx.moveTo(this.c.width / 2 + first.x, this.c.height / 2 - first.y);
    #       tail.forEach((tail) => {
    #         this.ctx.lineTo(this.c.width / 2 + tail.x, this.c.height / 2 - tail.y);
    #       });
    #
    #       this.ctx.closePath();
    #       this.ctx.fill();
    #     })
    #   }
    # }