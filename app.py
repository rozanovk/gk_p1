import sys
from PyQt4 import QtGui, QtCore


class MyView(QtGui.QGraphicsView):
    def __init__(self):
        QtGui.QGraphicsView.__init__(self)

        self.scene = QtGui.QGraphicsScene(self)
        self.scene.setSceneRect(QtCore.QRectF(0, 0, 245, 245))

        self.setScene(self.scene)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    view = MyView()
    view.show()
    sys.exit(app.exec_())



# let cube1      = new Cuboid(25, -20, 50, 25, 25, 'red');
# let cube2      = new Cuboid(-50, -20, 50, 25, 25, 'green');
# let street     = new Street(-15, -20, 40, 25, 55, 'gray');
# let cuboid     = new Cuboid(-45, -20, 105, 90, 40, 'purple');
# let scene      = new Scene('camera3d', [cube1, cube2, street, cuboid])
# let controller = new Controller(scene);
