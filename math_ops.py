import numpy as np


class Rotation:
    def __init__(self, vectors, direction):
        self.vectors = vectors
        self.angle = direction * np.pi / 90
        self.matrices = {"x": self.x_rotation_matrix,
                         "y": self.y_rotation_matrix,
                         "z": self.z_rotation_matrix
                         }

    def x_rotation_matrix(self):
        return [[1, 0, 0, 0], [0, np.cos(self.angle), -1 * np.sin(self.angle), 0],
                 [0, np.sin(self.angle), np.cos(self.angle), 0], [0, 0, 0, 1]]

    def y_rotation_matrix(self):
        return [[np.cos(self.angle), 0, np.sin(self.angle), 0], [0, 1, 0, 0],
                [-1 * np.sin(self.angle), 0, np.cos(self.angle), 0], [0, 0, 0, 1]]

    def z_rotation_matrix(self):
        return [[np.cos(self.angle), -1 * np.sin(self.angle), 0, 0], [np.sin(self.angle), np.cos(self.angle), 0, 0],
                [0, 0, 1, 0], [0, 0, 0, 1]]

    def rotate(self, coordinate):
        for vector in self.vectors:
             a = vector.a.to_vector()
             b = vector.b.to_vector()

             a = np.multiply(self.matrices[coordinate](), a)
             b = np.multiply(self.matrices[coordinate](), b)

             vector.a.update_from_vector(a)
             vector.b.update_from_vector(b)


class Translation:
    def __init__(self, vectors):
        self.vectors = vectors
        self.step = 2

    def move_up(self):
        for vector in self.vectors:
            vector.a.y -= self.step
            vector.b.y -= self.step

    def move_down(self):
        for vector in self.vectors:
            vector.a.y += self.step
            vector.b.y += self.step

    def move_left(self):
        for vector in self.vectors:
            vector.a.x += self.step
            vector.b.x += self.step

    def move_right(self):
        for vector in self.vectors:
            vector.a.x -= self.step
            vector.b.x -= self.step

    def move_forward(self):
        for vector in self.vectors:
            vector.a.z -= self.step
            vector.b.z -= self.step

    def move_back(self):
        for vector in self.vectors:
            vector.a.z += self.step
            vector.b.z += self.step

