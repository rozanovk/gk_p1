class PaintersAlgorithm:
    def compare(self, p1, p2):
        if p1.max_z() < p2.min_z():
            return 1
        if p1.dist_to_observator() > p2.dist_to_observator():
            return -1
        if p1.dist_to_observator() < p2.dist_to_observator():
            return -1
        else:
            return 0